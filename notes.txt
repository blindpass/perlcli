1. "decompress" passwords by expanding into an alphabet N bits
   wide, where each character occurs in output alphabet a number
   of times in proportion to its probability, ideally using a
   Markov model for character frequency.  Pick a random equivalent
   output character for each input one, effectively whitening the
   password, so any random block of data is equally believable.

2. Hash a master secret with a friendly name of the service, like
   "yahoo" or "gmail", for an exponentially-distributed random
   number of trials (probability p after each round of stopping)
   to store.  Store data at that hash address.  This way not finding
   the stored password after N trials does not guarantee that it
   does not exist in the database, and collisions are more likely.

3. Password is also encrypted with a key that's dependent on
   location within the database, friendly name, and number of rounds
   hashed to find that location, so arriving at the same location
   serrendipidously by a different hash path will result in a
   completely different password.

Not possible to list the services for which passwords are stored.
Not possible to verify how many services are stored at all (fuzzing).
Any master secret will yield valid accesses to the database
(deniability); wrong password will yield wrong services and wrong
passwords.

How many keys are needed to be derived from the "master" secret?
How are they used?
